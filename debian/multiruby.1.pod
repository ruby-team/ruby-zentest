=head1 NAME

multiruby - runs anything you want on multiple versions of ruby

=head1 SYNOPSIS

B<multiruby> I<./TestMyProject.rb>

=head1 DESCRIPTION

B<multiruby> runs anything you want on multiple versions of ruby. Great
for compatibility checking! Use I<multiruby_setup> to manage your
installed versions.

=head1 AUTHOR

Ryan Davis, Eric Hodel, seattle.rb

This manual page is written by Praveen Arimbrathodiyl <praveen@debian.org> for
Debian GNU System (GNU/Linux, GNU/kFreeBSD, GNU/Hurd).
