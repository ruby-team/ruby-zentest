Source: ruby-zentest
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Pirate Praveen <praveen@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               ruby-minitest
Standards-Version: 3.9.7
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-zentest.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-zentest
Homepage: https://github.com/seattlerb/zentest
XS-Ruby-Versions: all

Package: ruby-zentest
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby:any | ruby-interpreter,
         ${misc:Depends},
         ${shlibs:Depends}
Description: ZenTest provides 4 different tools: zentest, unit_diff, autotest, and multiruby
 zentest scans your target and unit-test code and writes your missing
 code based on simple naming rules, enabling XP at a much quicker pace.
 zentest only works with Ruby and Minitest or Test::Unit. There is
 enough evidence to show that this is still proving useful to users, so
 it stays.
 .
 unit_diff is a command-line filter to diff expected results from
 actual results and allow you to quickly see exactly what is wrong.
 Do note that minitest 2.2+ provides an enhanced assert_equal obviating
 the need for unit_diff
 .
 autotest is a continuous testing facility meant to be used during
 development. As soon as you save a file, autotest will run the
 corresponding dependent tests.
 .
 multiruby runs anything you want on multiple versions of ruby. Great
 for compatibility checking! Use multiruby_setup to manage your
 installed versions.
